//funcao que faz um bloco aparecer ou desaparecer
function display(nomeBloco) {
    let buscarBloco = document.querySelector(nomeBloco);
    if (buscarBloco.style.display === "flex") {
        buscarBloco.style.display = "none";
    }
    
     else{
         return buscarBloco.style.display = "flex";
        }
    }

//Esconde ou mostra um bloco ao ser clicado
function addClickDisplay(botao, bloco) {
    document.querySelector(botao).addEventListener("click", function () {
        display(bloco)
    })
}



addClickDisplay(`#regras`, `.regras`)
addClickDisplay("#iniciarJogo", ".corpo-jogo")
//Cria a div da torre

function criarTorre(IDdaDiv) {
    let pegarBloco = document.querySelector(".corpo-jogo")
    let criarTorre = document.createElement("div")
    criarTorre.setAttribute("id", IDdaDiv)
    criarTorre.setAttribute("class", "coluna-do-jogo flex-col-reverse")
    return pegarBloco.appendChild(criarTorre)
}

//Cria as 3 torres iniciais
criarTorre("start")
criarTorre("offset")
criarTorre("end")

//Cria cada disco e os adiciona a primeira torre
function criarBloco(IDdaDivPai, numeroDaPeca) {
    let pegarBloco = document.querySelector(IDdaDivPai)
    let criarTorre = document.createElement("div")
    criarTorre.setAttribute("id", numeroDaPeca)
    criarTorre.setAttribute("class", "bloco-do-jogo")
    return pegarBloco.appendChild(criarTorre)
}

//Cria a coluna de fundo no qual os discos são inseridos 
function criarBarraDeFundo(IDdaDivPai) {
    let pegarBloco = document.querySelector(IDdaDivPai)
    let criarTorre = document.createElement("div")
    criarTorre.setAttribute("class", "barra-de-fundo")
    return pegarBloco.appendChild(criarTorre)
}

//Criação do elementos do jogo
criarBarraDeFundo("#start")
criarBarraDeFundo("#offset")
criarBarraDeFundo("#end")
criarBloco("#start", "pecaQuatro")
criarBloco("#start", "pecaTres")
criarBloco("#start", "pecaDois")
criarBloco("#start", "pecaUm")


const divErro = document.getElementById(`mensagemErro`)

const torre1 = document.getElementById('start');
const torre2 = document.getElementById('offset');
const torre3 = document.getElementById('end');
let numJogadas=0
let primeiraTorreSelecionada
let segundaTorreSelecionada
let jogadasMelhorPartida = 0

//Função que muda os discos de torre conforme click
function mudarDiscos(primeiraTorre, segundaTorre) {
    const widthTorre1 = primeiraTorre.lastElementChild.clientWidth;
    const widthTorre2 = segundaTorre.lastElementChild.clientWidth;
      
    if (widthTorre1 < widthTorre2 || segundaTorre.childElementCount == 1) {
        segundaTorre.appendChild(primeiraTorre.lastElementChild)
    }

    else if (widthTorre1 > widthTorre2) {
        divErro.innerHTML="Uma peça maior não pode ficar acima de uma menor."
        adicionarOuRemoverBorda(primeiraTorreSelecionada,false)
    }
}

//função que altera a borda do disco selecionado 
function adicionarOuRemoverBorda(qualTorre,trueOrFalse){
    if (trueOrFalse==true){
        return qualTorre.lastElementChild.style.border= `5px solid orange`
    }
return  document.querySelector(`#` + qualTorre.lastElementChild.id).style.removeProperty(`border`)
}

//Função que reinicia a seleção das torres
function reiniciarSelecao() {
    document.querySelector(`#` + segundaTorreSelecionada.lastElementChild.id).style.removeProperty(`border`);
    numJogadas += 1;
    document.getElementById('numeroDeJogadas').innerHTML = `<p>Nº de movimentos : ${numJogadas}</p>`;
    primeiraTorreSelecionada = null;
    segundaTorreSelecionada = null;
}
//Declara vencedor e chama funcao que reiniciara o jogo
function declararVencedor(numDiscosNaTorre3){
    if (numDiscosNaTorre3 === 5) {
        display(`#jogarDeNovo`)
        divErro.innerHTML= `Parabéns!!! Você ganhou, quer tentar de novo?`
        document.getElementById("end").style.pointerEvents= "none";
      return  novoJogo()
    }


}

//Função que recebe a seleção das torres e executa a mudança de discos e reinicia as seleções
function torreSelecionada(torre) {

    if (primeiraTorreSelecionada == null && torre.childElementCount > 1) {
        primeiraTorreSelecionada = torre
        divErro.innerHTML= ``
        return adicionarOuRemoverBorda(primeiraTorreSelecionada,true)
    }

    if (segundaTorreSelecionada == null && primeiraTorreSelecionada != null) {
        segundaTorreSelecionada = torre

        mudarDiscos(primeiraTorreSelecionada, segundaTorreSelecionada)
        adicionarOuRemoverBorda(segundaTorreSelecionada,false)
        reiniciarSelecao()       
        declararVencedor(torre3.childElementCount)

    }
}

//Event listner que dispara a função de movimentar os discos da torre selecionada
function receberClick(coluna, torre) {
    document.querySelector(coluna).addEventListener("click", function () {
        torreSelecionada(torre)
    })
}

receberClick('#start', torre1)
receberClick('#offset', torre2)
receberClick('#end', torre3)

//Reinicia o jogo ao estado inicial e guarda o numero de jogadas gastas para ganhar
function novoJogo(){
document.querySelector(`#jogarDeNovo`).addEventListener("click", function () {
    torre1.innerHTML=`<div class="barra-de-fundo"></div>`
    torre2.innerHTML=`<div class="barra-de-fundo"></div>`
    torre3.innerHTML=`<div class="barra-de-fundo"></div>`

criarBloco("#start", "pecaQuatro")
criarBloco("#start", "pecaTres")
criarBloco("#start", "pecaDois")
criarBloco("#start", "pecaUm")
display(`#jogarDeNovo`)
divErro.innerHTML= ``
document.getElementById("end").style.pointerEvents= "auto";
if(numJogadas<jogadasMelhorPartida && numJogadas> 0 || jogadasMelhorPartida==0) {jogadasMelhorPartida=numJogadas
    document.getElementById('melhorPartida').innerHTML=`Número de jogadas na sua melhor partida = ${jogadasMelhorPartida}` }
numJogadas=0
document.getElementById('numeroDeJogadas').innerHTML=``
})
}
